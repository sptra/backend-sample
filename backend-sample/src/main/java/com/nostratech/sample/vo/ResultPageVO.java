package com.nostratech.sample.vo;

/**
 * Created by fani on 4/23/15.
 */
public class ResultPageVO extends ResultVO {

    private String pages;
    private String elements;

    public ResultPageVO() {
    }

    public ResultPageVO(String pages, String elements) {
        this.pages = pages;
        this.elements = elements;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getElements() {
        return elements;
    }

    public void setElements(String elements) {
        this.elements = elements;
    }

}
