# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Sample Backend using Java, Hibernate, Spring Boot, Spring Data Repository + MySQL

### Prerequisites ###

* Install Java 7+
* Install DB (PostgreSQL / MySQL)
* Install Maven

### How do I get set up? ###

* Run PostgreSQL / MySQL database
* Create database sample for user root Or you can change db properties on configuration/jdbc.properties
* Add jdbc driver for PostgreSQL in pom.xml
* Change jdbc provider on configuration/jdbc.properties
* Clone this repo
* Run mvn spring-boot:run
* Open browser
* Type http://localhost:8080/swagger-ui/index.html

### Who do I talk to? ###

* Please contact me at agus@nostratech.com